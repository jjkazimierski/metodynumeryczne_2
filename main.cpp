#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <fstream>
#include <string>

int it;
int iter;

double eps;

int warunek_stopu; // 0 - iteracje (domyślnie), 1 - epsilon

int * ilosc_rownan;
double **rownania;
double *X;
double *Y;
//vector< vector<int> > vec;

using namespace std;

/* beautify functions */
void linia()
{
	for(int i = 1; i < 64; i++) cout << "-";
	cout << endl;
}

void msg(string _msg)
{
	linia();
	cout << _msg << endl;
	linia();
}

void msg_wybor(string _msg, string o1, string o2)
{
	linia();
	cout << _msg << endl;
	cout << o1 << endl;
	cout << o2 << endl;
	linia();
}

/* read & display data functions */
bool wczytaj_rownania()
{
	ifstream file ("input");

	if ( !file.is_open() )
	{
		return false;
	}
	else
	{
		for(int r = 0; r < (*ilosc_rownan); r++)
		{
			for(int c = 0; c < (*ilosc_rownan) + 1; c++)
			{
				file >> rownania[r][c];
			}
		}
	}
	return true;
}

void wyswietl_rownania()
{
	for(int r = 0; r < (*ilosc_rownan); r++)
	{
		for(int c = 0; c < (*ilosc_rownan) + 1; c++)
		{
			if( c == (*ilosc_rownan) ) cout << " |  ";
			cout << "[" << rownania[r][c] << "] ";
		}
		cout << endl;
	}
	linia();
}

void wyswietl_wynik()
{
		for(int c = 0; c < (*ilosc_rownan); c++)
		{
			cout << "x" << c+1 << " = " << X[c] << ",  ";
		}
		cout << endl;
	linia();
}

bool stop()
{
	if(warunek_stopu == 0)
	{
		// iteracje
		if(iter > 0) return true; // pętla trwa

		/* if na brak zmian*/
	}
	if(warunek_stopu == 1)
	{
		// epsilon
		if(it <= 0) return true; // pętla trwa
	}

	return false; // pętla zakończona
}
/*
bool diagonal_dominant()
{
	int sum;
	for(int r = 0; r < (*ilosc_rownan); r++)
	{
		sum = 0;
		for(int c = 0; c < (*ilosc_rownan); c++)
		{
			if( r != c ) sum += abs(rownania[r][c]);
		}
		if( abs(rownania[r][r]) <= sum) return false; // diagonalDominant
	}

	return true;
}


bool symmetric()
{
	for(int r = 0; r < (*ilosc_rownan); r++)
	{
		for(int c = 0; c < (*ilosc_rownan); c++)
		{
			if( rownania[r][c] != rownania[c][r] ) return false;
		}
	}

	return true;
}
*/

bool positive_definite()
{
	for(int r = 0; r < (*ilosc_rownan); r++)
	{
			if( rownania[r][r] < 0 ) return false;
	}

	return true;
}

int main() {

	int ir;

	msg("Program wczyta dane z pliku: ./input");
	msg("Podaj liczbę równań:");

	cin >> ir;
	ilosc_rownan = &ir;

	rownania = new double*[ir];
	for(int i = 0; i < ir; i++) rownania[i] = new double[ir+1];
	X = new double[ir];
	Y = new double[ir];

  if( !wczytaj_rownania() )
	{
		msg("Blad: Nie mozna wczytac pliku.");
	}
	else
	{
		msg("Wczytano dane:");
		wyswietl_rownania();

		msg_wybor("Wybierz warunek stopu:", "0 - iteracje", "1 - dokladnosc wyniku");
		cin >> warunek_stopu;

		if(warunek_stopu)
		{
			msg("Podaj dokladnosc wyniku:");
			cin >> eps;
		}
		else
		{
			msg("Podaj ilosc iteracji:");
			cin >> it;
			iter = it;
		}

		if( positive_definite() )
		{
			do
			{
				for (int i = 0; i < (*ilosc_rownan); i++)
				{
					Y[i] = rownania[i][(*ilosc_rownan)] / rownania[i][i];
					for (int j = 0; j < (*ilosc_rownan); j++)
					{
		 				if (j == i) continue;
						Y[i] = Y[i] - ((rownania[i][j] / rownania[i][i]) * X[j]);
						X[i] = Y[i];
					}
				}
				wyswietl_wynik();
				--iter;
			} while(stop());
		}
		else
		{
			msg("Podana macierz nie spełnia kryteriów zbieżności.");

			if( !positive_definite() ) msg("test");
		}
	}
	msg("Program wykonany. Zamknij aplikacje.");
	return 0;
}
